# vfb_activex_control

#### 介绍
VisualFreeBasic开发系统(如下简称VFB)ActiveX组件调用自动化控件，作者是网友“驰骋乾坤”大神。

#### 软件架构
我们以Codejock.Controls.v15.3.1.ocx为例子
控件在选择Codejock.Controls.v15.3.1.ocx后，自动在VFB的根目录生成该OCX的声明文件：
1. Private\XtremeSuiteControls\
+ Consts.inc 枚举声明 
+ XtremeSuiteControls.inc 调用接口 
2. Control\ActiveX\XtremeSuiteControls\ 
+ CheckBox.bi CheckBox事件接口声明（类似.h）
+ CheckBox.inc CheckBox事件接口实现(类似.cpp)
+ ColorPicker.bi ColorPicker事件接口声明 
+ ColorPicker.inc ColorPicker事件接口实现 

如此类推


#### 安装教程

1.  checkout 此项目到VFB根目录下的Control\ActiveX文件夹
2.  打开code/ActiveX_dll.ffp项目
3.  编译控件，重启VFB

#### 使用说明

1.  新建演示项目，例如TestOCX
2.  在控件栏目拖名称为：ActiveX的控件到窗体（图标为四叶草）
![输入图片说明](https://foruda.gitee.com/images/1681044182313686992/38113b05_1451385.png "屏幕截图")

3.  选择Dll或者ActiveX文件

![输入图片说明](https://foruda.gitee.com/images/1681044236887583880/429e6245_1451385.png "屏幕截图")

4. 选择主接口（例如Codejock.Controls.v15.3.1.ocx有CheckBox组件、ColorPicker、FlatEdit组件，现在要加一个文本框（FlatEdit），主接口就选择FlatEditEvents接口 ）


#### 参与贡献

1.  Fork 本仓库
2.  新建 vfb_activex_control_xxx 分支
3.  提交代码
4.  新建 Pull Request
