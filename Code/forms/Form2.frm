﻿#VisualFreeBasic_Form#  Version=5.8.5
Locked=0

[Form]
Name=Form2
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=选择主接口
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=531
Height=349
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=False
MinimizeBox=False
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ListView]
Name=ListView1
Index=-1
Style=1 - 报表视图
BStyle=1 - 细边框
Theme=False
ImageNormalS=无图像列表控件
ImageSmallS=无图像列表控件
SingleSel=True
ShowSel=True
Sort=0 - 不排序
SortColumn=False
NoScroll=False
AutoArrange=False
EditLabels=False
OwnerData=False
AlignTop=False
AlignLeft=False
OwnDraw=False
ColumnHeader=True
ColumnEditor=接口||0|150|GUID||0|350|
NoSortHeader=False
BorderSelect=False
Check=False
FlatScrollBars=False
FullRowSelect=True
GridLines=True
HeaderDragDrop=False
InfoTip=False
LabelTip=False
MultiWorkAreas=False
OneClickActivate=False
Regional=False
SimpleSelect=False
SubItemImages=False
TrackSelect=False
TwoClickActivate=False
UnderlineCold=False
UnderlineHot=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
TextBkColor=SYS,5
Font=微软雅黑,9,0
Left=6
Top=5
Width=504
Height=299
Layout=5 - 宽度和高度
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

Dim Shared Form2_UserData As Integer 
Sub Form2_Shown(hWndForm As hWnd ,UserData As Integer) '窗口完全显示后。
   'hWndForm  当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'UserData  来自显示窗口最后1个参数，例： Form2.Show(父窗口句柄,模式,UserData)
   Dim ip As InterfacesParam Ptr = UserData
   Form2_UserData = UserData
   Dim lsAll() As String ,ls() As String
   
   ListView1.ColumnWidth(0)  = 150
   ListView1.ColumnWidth(1) = ListView1.Width - 150 - 22
   
   ip->ReturnValue = ip->Value
   
   Dim nSelected As Integer = 0
   For i As Integer = 1 To Split(ip->AllList ,"," ,lsAll())
      Split(lsAll(i) ,"|" ,ls())        
      
      ListView1.AddItemColList 0 ,0 ,ls(1) ,ls(2)
      
      If lsAll(i) = ip->Value Then
         nSelected = i - 1
      End If
   Next i  
   
   ListView1.SelectedItem = nSelected
   ListView1.ScrollSelectedItem()
End Sub

Sub Form2_ListView1_WM_LButtonDblclk(hWndForm As hWnd, hWndControl As hWnd, MouseFlags As Long, xPos As Long, yPos As Long)  '双击鼠标左键
   'hWndForm    当前窗口的句柄(WIN系统用来识别窗口的一个编号，如果多开本窗口，必须 Me.hWndForm = hWndForm 后才可以执行后续操作本窗口的代码)
   'hWndControl 当前控件的句柄(也是窗口句柄，如果多开本窗口，必须 Me.控件名.hWndForm = hWndForm 后才可以执行后续操作本控件的代码 )
   'MouseFlags  MK_CONTROL   MK_LBUTTON     MK_MBUTTON     MK_RBUTTON    MK_SHIFT     MK_XBUTTON1       MK_XBUTTON2 
   ''           CTRL键按下   鼠标左键按下   鼠标中键按下   鼠标右键按下  SHIFT键按下  第一个X按钮按下   第二个X按钮按下
   '检查什么键按下用  If (MouseFlags And MK_CONTROL)<>0 Then CTRL键按下 
   'xPos yPos   当前鼠标位置，相对于控件。就是在控件里的坐标。
   Dim nIndex As Integer = ListView1.SelectedItem 
   If nIndex > 0 Then
      Dim ip As InterfacesParam Ptr = Form2_UserData     
      ip->ReturnValue = ListView1.GetItemText(nIndex ,0) + "|" + ListView1.GetItemText(nIndex ,1)
      Me.Close   
   End If
    
End Sub





