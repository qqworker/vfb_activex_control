﻿#include Once "Afx/CAtlCon.inc"
#include Once "ClsControl.inc"
Type Event_Active Extends Object
   Declare Virtual Function QueryInterface(ByVal riid As REFIID ,ByVal ppvObject As LPVOID Ptr) As HRESULT
   Declare Virtual Function AddRef()  As ULong
   Declare Virtual Function Release() As ULong
   EventIID As IID
   dwCookie As DWORD
   index    As Long
   Declare Constructor()
   Private : 
   cRef As ULong
   
end Type
Constructor Event_Active
End Constructor

Type Event_ActiveX Extends Event_Active
   Declare Virtual Function GetTypeInfoCount(ByVal pctinfo As UINT Ptr) As HRESULT
   Declare Virtual Function GetTypeInfo(ByVal iTInfo As UINT ,ByVal lcid As LCID ,ByVal ppTInfo As ITypeInfo Ptr Ptr) As HRESULT
   Declare Virtual Function GetIDsOfNames(ByVal riid As Const IID Const Ptr ,ByVal rgszNames As LPOLESTR Ptr ,ByVal cNames As UINT ,ByVal LCID As LCID ,ByVal rgDispId As DISPID Ptr) As HRESULT
   Declare Virtual Function Invoke(ByVal dispIdMember As DISPID ,ByVal riid As Const IID Const Ptr ,ByVal LCID As LCID ,ByVal wFlags As WORD ,ByVal pDispParams As DISPPARAMS Ptr ,ByVal pVarResult As VARIANT Ptr ,ByVal pExcepInfo As EXCEPINFO Ptr ,ByVal puArgErr As UINT Ptr) As HRESULT
   Declare Constructor()
   OnEvent  As Sub(ByVal ID As DISPID ,ByVal param As VARIANT Ptr ,ByVal pVarResult As VARIANT Ptr)
   IOnEvent As Sub(ByVal Index As Long ,ByVal ID As DISPID ,ByVal param As VARIANT Ptr ,ByVal pVarResult As VARIANT Ptr)

End Type
Constructor Event_ActiveX
End Constructor

Function Event_Active.QueryInterface(ByVal riid As REFIID ,ByVal ppvObject As LPVOID Ptr) As HRESULT
   If ppvObject = Null Then Return E_INVALIDARG
   If IsEqualIID(riid ,@EventIID) Or IsEqualIID(riid ,@IID_IUnknown) Or IsEqualIID(riid ,@IID_IDispatch) Then
       *ppvObject = @This
      Cast(Afx_IUnknown Ptr , *ppvObject)->AddRef
      Return S_OK
   End If
   Return E_NOINTERFACE
End Function

Function Event_Active.AddRef() As ULong
   This.cRef += 1
   Function  = This.cRef
End Function

Function Event_Active.Release() As ULong
   This.cRef -= 1
   Function  = This.cRef
End Function

Function Event_ActiveX.GetTypeInfoCount(ByVal pctinfo As UINT Ptr) As HRESULT
    *pctinfo = 0
   Return E_NOTIMPL
End Function

Function Event_ActiveX.GetTypeInfo(ByVal iTInfo As UINT ,ByVal LCID As LCID ,ByVal ppTInfo As ITypeInfo Ptr Ptr) As HRESULT
   Return E_NOTIMPL
End Function

Function Event_ActiveX.GetIDsOfNames(ByVal riid As Const IID Const Ptr ,ByVal rgszNames As LPOLESTR Ptr ,ByVal cNames As UINT ,ByVal LCID As LCID ,ByVal rgDispId As DISPID Ptr) As HRESULT
   Return E_NOTIMPL
End Function

Function Event_ActiveX.Invoke(ByVal dispIdMember As DISPID ,ByVal riid As Const IID Const Ptr ,ByVal LCID As LCID ,ByVal wFlags As WORD ,ByVal pDispParams As DISPPARAMS Ptr ,ByVal pVarResult As VARIANT Ptr ,ByVal pExcepInfo As EXCEPINFO Ptr ,ByVal puArgErr As UINT Ptr) As HRESULT
   If This.index = -1 Then
      If OnEvent Then OnEvent(dispIdMember ,pDispParams->rgvarg ,pVarResult)
   Else
      If IOnEvent Then IOnEvent(This.index ,dispIdMember ,pDispParams->rgvarg ,pVarResult)
   End If
   Return S_OK
End Function

Type Class_ActiveX Extends Class_Control
   Declare Property Object() As IDispatch Ptr
   Declare Property Object(Rh As IDispatch Ptr)
   Declare Operator Cast() As Any Ptr
   Declare Function AppendControl(ByVal pWindow As CWindow Ptr ,ByVal cID As Integer ,ByRef wszLibName As Const Wstring ,ByRef sclsid As CWSTR  ,ByRef wszLicKey As Wstring = "" ,ByVal x As Long = 0 ,ByVal y As Long = 0 ,ByVal nWidth As Long = 0 ,ByVal nHeight As Long = 0 ,ByVal dwStyle As DWORD = WS_VISIBLE Or WS_CHILD Or WS_TABSTOP ,ByVal dwExStyle As DWORD = 0) As .hWnd
   Declare Sub WithEvents(ByRef pEvtObj As Event_Active)
   Declare Sub UnWithEvents(ByRef pEvtObj As Event_Active)
   Declare Constructor()
   
   Declare Sub SetEventProc(ByVal index As Long ,mOnEvent As Any Ptr)
   Declare Function Create() As IDispatch Ptr
   Declare Function ActiveObject() As IDispatch Ptr
   Declare Function Clear() As Long
   'atl         As CAtlCon
   rIID        As IID
   sPath       As String
   EventObject As Event_Active Ptr
   mHwnd As .hWnd
   Private : 
   m_hAtlLib As HMODULE
   mobject As IDispatch Ptr
   IsControl As BOOLEAN
   AtlGetControl As Function(ByVal h As .hWnd ,ByRef punk As IUnknown Ptr) As HRESULT
   AtlAttachControl As Function(ByVal pControl As IUnknown Ptr ,ByVal hCtl As .hWnd ,ByVal ppUnkContainer As IUnknown Ptr Ptr) As HRESULT
   AtlWinInit As Function () As BOOLEAN
End Type
Operator Class_ActiveX.Cast() As Any Ptr
   Return mobject
End Operator
Function Class_ActiveX.Clear() As Long
   If mobject Then
      Return mobject->lpVtbl->Release(mobject)
      'IDispatch_Release(mobject)
   End If
End Function

Function Class_ActiveX.Create() As IDispatch Ptr
   Dim hr As HRESULT = CoInitializeEx(NULL ,COINIT_APARTMENTTHREADED) ' CoInitialize(NULL)
   If CoCreateInstance(@riid ,NULL ,CLSCTX_SERVER ,@iid_idispatch ,@mobject) <> 0 Then Exit Function
   WithEvents( *EventObject)
   Return mobject
End Function
   
Function Class_ActiveX.ActiveObject() As IDispatch Ptr
   Dim hr As HRESULT = CoInitializeEx(NULL ,COINIT_APARTMENTTHREADED)
   Dim punk As IUnknown Ptr
   If GetActiveObject(@riid ,NULL ,@punk) <> 0 Then Exit Function
   If punk Then punk->lpVtbl->QueryInterface(punk, @IID_IDispatch, @mobject)
   WithEvents( *EventObject)

   'mobject->lpVtbl->AddRef(mobject)
   Return mobject
End Function

Property Class_ActiveX.Object(Rh As IDispatch Ptr)'Let
   mobject = Rh
   WithEvents(*EventObject)
End Property

Sub Class_ActiveX.UnWithEvents(ByRef pEvtObj As Event_Active)
   
End Sub

Sub Class_ActiveX.WithEvents(ByRef pEvtObj As Event_Active)
   '引用事件 参数：被引用事件组件对象如Excelapp,事件对象（需要创建CEvent或CIUnknown对象）
   EventObject = @pEvtObj
   If This.Object = NULL Then Exit Sub
   Dim m_dwCookie As DWORD = 0
   Dim pCPC       As IConnectionPointContainer Ptr
   Dim hr         As HRESULT = IUnknown_QueryInterface(This.Object ,@IID_IConnectionPointContainer ,@pCPC)
   If hr <> S_OK Or pCPC = NULL Then Return
   Dim pCP  As IConnectionPoint Ptr
   Dim riid As IID = pEvtObj.EventIID
   hr = pCPC->lpvtbl->FindConnectionPoint(pCPC ,@riid ,@pCP)
   If hr <> S_OK Or pCP = NULL Then
      Dim oEnm As IEnumConnectionPoints Ptr
      hr = pCPC->lpVtbl->EnumConnectionPoints(pCPC ,@oEnm)
      If oEnm = NULL Then Return
      Do While oEnm->lpVtbl->Next(oEnm ,1 ,@pCP ,NULL) = 0
         If pCP = NULL Then Return
         hr = pCP->lpVtbl->GetConnectionInterface(pCP ,@riid)
      Loop
      If hr = 0 Then
         pEvtObj.EventIID = riid
      Else
         IUnknown_Release(pCPC)
         Return
      End If
   End If
   If pEvtObj.dwCookie Then hr = pCP->lpvtbl->Unadvise(pCP ,pEvtObj.dwCookie)
   If hr <> S_OK Or pCP = NULL Then
      IUnknown_Release(pCPC)
      Return
   End If
   pEvtObj.dwCookie = 0
   hr               = pCP->lpvtbl->Advise(pCP ,Cast(Any Ptr ,@pEvtObj) ,@m_dwCookie)
   If hr <> S_OK Or pCP = NULL Then
      IUnknown_Release(pCPC)
      Return
   End If
   pEvtObj.dwCookie = m_dwCookie
   IUnknown_Release(pCPC)
   IUnknown_Release(pCP)
End Sub

Function Class_ActiveX.AppendControl(ByVal pWindow As CWindow Ptr, ByVal cID As Integer, ByRef wszLibName As Const Wstring, ByRef sclsid As CWSTR, ByRef wszLicKey As Wstring = "", ByVal x As Long = 0, ByVal y As Long = 0, ByVal nWidth As Long = 0, ByVal nHeight As Long = 0, ByVal dwStyle As DWORD = WS_VISIBLE Or WS_CHILD Or WS_TABSTOP, ByVal dwExStyle As DWORD = 0) As .hWnd
   sPath = wszLibName
   Dim rclsid As CLSID, s As Wstring Ptr = sclsid
   Dim hr     As HRESULT = CLSIDFromString(s, @rclsid)
   
   rIID    = rclsid
   mobject = AfxNewCom(wszLibName, rclsid, IID_IDispatch, wszLicKey)
   'If mobject =NULL Then Return 0
   Dim o As Any Ptr
   If mobject Then hr = mobject->lpVtbl->QueryInterface(mobject, @IID_IOlewindow, @o)
   '   Debug.Print mobject
   If o <> NULL AndAlso mobject <> NULL AndAlso hr = S_OK Then
      If m_hAtlLib = NULL Then Return 0
      Dim h As .hWnd = pWindow->AddControl("AtlAxWin", pWindow->hWindow, cID, "", x, y, nWidth, nHeight, dwStyle, dwExStyle)
      'Print h
      AtlAttachControl(Cast(Any Ptr, mobject), h, NULL)
      IsControl = True
      Return h 'atl.AddControl(pWindow ,cID ,wszLibName ,rclsid ,IID_IDispatch ,wszLicKey ,x ,y ,nWidth ,nHeight ,dwStyle ,dwExStyle)
   Else
      IsControl = False
      Dim title As CWSTR = wszLibName
      Return pWindow->AddControl("Label", pWindow->hWindow, cID, title, x, y, nWidth, nHeight, dwStyle, dwExStyle)
   End If
   
End Function

Sub Class_ActiveX.SetEventProc(ByVal index As Long ,mOnEvent As Any Ptr) '单独控件事件回调 Sub OnEvent(ByVal ID As DISPID ,ByVal param As VARIANT Ptr ,ByVal pVarResult As VARIANT Ptr)
   '控件组事件回调 Sub IOnEvent(ByVal Index As Long ,ByVal ID As DISPID ,ByVal param As VARIANT Ptr ,ByVal pVarResult As VARIANT Ptr)
   If This.Object = NULL Then Exit Sub
   Static ActiveXsink() As Event_ActiveX
   If index = -1 Then
      ReDim Preserve ActiveXsink(0)
      ActiveXsink(0).index   = index
      ActiveXsink(0).OnEvent = mOnEvent
      WithEvents(Activexsink(0))
   Else
      If UBound(ActiveXsink) < index Then ReDim Preserve ActiveXsink(index)
      Activexsink(index).index    = index
      Activexsink(index).IOnEvent = mOnEvent
      WithEvents(Activexsink(index))
   End If
End Sub

Constructor Class_ActiveX()
   Dim hr As HRESULT = OleInitialize(NULL)
   m_hAtlLib = LoadLibraryW("Atl.dll")
   AtlWinInit = Cast(Any Ptr, GetProcAddress(m_hAtlLib, "AtlAxWinInit"))
   AtlAttachControl = Cast(Any Ptr ,GetProcAddress(m_hAtlLib ,"AtlAxAttachControl"))
   AtlGetControl = Cast(Any Ptr ,GetProcAddress(m_hAtlLib ,"AtlAxGetControl"))
   If AtlWinInit <> NULL Then AtlWinInit()
End Constructor

Property Class_ActiveX.Object() As IDispatch Ptr 'Get
   If This.mobject <> NULL Then
      'This.mobject->lpVtbl->AddRef(This.mobject)
      If IsControl Then
         Dim punk As IUnknown Ptr
         Dim hr   As HRESULT = AtlGetControl(hWndControl, punk)
         If punk Then punk->lpVtbl->QueryInterface(punk, @IID_IDispatch, @mobject)
      End If
   Else
      Dim punk As IUnknown Ptr
      Dim hr   As HRESULT = AtlGetControl(hWndControl, punk)
      If punk Then punk->lpVtbl->QueryInterface(punk, @IID_IDispatch, @mobject)
   End If
   Return This.mobject
End Property
'Property Class_ActiveX.NewObject() As IDispatch Ptr
'   If This.mobject <> NULL Then
'      This.mobject->lpVtbl->AddRef(This.mobject)
'   Else
'      This.mobject= atl.GetDispatch(hWndControl)
'      If This.mobject then This.mobject->lpVtbl->Release(This.mobject)
'   End If
'   'If This.mobject=NULL Then 
'   Return This.mobject
'End Property